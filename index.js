const cheerio = require('cheerio');
const request = require('request');
const fs = require('fs');

request('https://store.steampowered.com/app/1161490/MotoGP20/', (error, response, html) => {
    if(!error && response.statusCode == 200) {
        const $ = cheerio.load(html);

        const gameTitle = $('.apphub_AppName').text().trim();
        const gameDescription = $('.game_description_snippet').text().trim();

        var game = JSON.parse(`{ "title":"${gameTitle}", "description":"${gameDescription}"}`); 
        
        var gameToJson = JSON.stringify(game);
        fs.writeFile('gameTitles.json', gameToJson, 'utf-8', (error) => {
            if(error) console.log(error.message);
            console.log('Saved to file');
        });
    }
})